export type User = {
  id: string;
  name: string;
  gender: 0 | 1;
  birthday: String;
  age: Number;
  email: string;
  phone?: string;
};
