import { Button, Space } from 'antd';
import React from 'react';
import { useGetUsersQuery } from '../rtk';

type UserManagementProps = {};

const UserManagement = (props: UserManagementProps): JSX.Element => {
  const { data: users } = useGetUsersQuery();
  console.log('users: ', users);
  return (
    <div className="p-4">
      <h2 className="text-lg">TODO: Please Write Code here!</h2>
    </div>
  );
};

export default UserManagement;
