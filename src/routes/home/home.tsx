import { Button, Card, Input, message, Select, Space } from 'antd';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';
import { LogsContainer } from '../../components/LogsContainer';
import { isValidJSONString } from '../../utils';

const userApiDefinition = `
  + \`GET\` /users
  + \`GET\` /users/:id
  + \`POST\` /users
  + \`PUT\` /users/:id
  + \`Delete\` /users/:id
`;

const Home = (): JSX.Element => {
  const [method, setMethod] = useState('GET');
  const [isLoading, setIsLoading] = useState(false);
  const [id, setId] = useState<string | undefined>(undefined);
  const [pageSize, setPageSize] = useState(50);
  const [pageIndex, setPageIndex] = useState(1);
  const [body, setBody] = useState('');

  const isQueryAll = !id && method === 'GET';

  const doRequest = async () => {
    try {
      setIsLoading(true);
      const payload = body.replaceAll('\n', '');

      if (payload && !isValidJSONString(payload)) {
        throw 'request body must be a valid JSON';
      }

      const pageQuery = isQueryAll ? `?pageIndex=${pageIndex}&pageSize=${pageSize}` : '';

      const result = await fetch('users' + (id ? `/${id}` : '') + pageQuery, {
        method,
        body: method === 'GET' ? null : payload,
      });
      const data = await result.json();
      setIsLoading(false);
      console.log(data);
    } catch (e) {
      setIsLoading(false);
      message.error('Request failed: ' + e);
    }
  };

  return (
    <div className="p-4">
      <Card title="🙅 Task1: User Management Requirements">
        <dl>
          <dt>Mock up</dt>
          <dd>A sea serpent.</dd>

          <dt>API</dt>
          <dd>
            <ReactMarkdown children={userApiDefinition}></ReactMarkdown>
          </dd>
          <dt>API Test</dt>
          <dd>
            <Space direction="vertical" className="mt-4 w-full">
              <Select value={method} onChange={(v) => setMethod(v)} style={{ width: 120 }}>
                <Select.Option value="GET">GET</Select.Option>
                <Select.Option value="POST">POST</Select.Option>
                <Select.Option value="PUT">PUT</Select.Option>
                <Select.Option value="DELETE">DELETE</Select.Option>
              </Select>
              <Input
                addonBefore="id"
                type="text"
                value={id}
                onChange={(v) => setId(v.target.value)}
                allowClear
              />
              {isQueryAll && (
                <Input
                  addonBefore="pageIndex"
                  type="number"
                  value={pageIndex}
                  min={1}
                  max={100}
                  onChange={(v) => setPageIndex(Number(v.target.value))}
                />
              )}
              {isQueryAll && (
                <Input
                  addonBefore="pageSize"
                  type="number"
                  value={pageSize}
                  min={1}
                  max={500}
                  onChange={(v) => setPageSize(Number(v.target.value))}
                />
              )}
              <Input.TextArea
                rows={10}
                value={body}
                onChange={(e) => setBody(e.target.value)}
                placeholder={`type request body here, must be a valid json like this:
                    {"name":"rio shi"}
                `}
                allowClear
              ></Input.TextArea>
              <Button type="primary" onClick={doRequest} disabled={isLoading} loading={isLoading}>
                Request
              </Button>
              Console, You can find result here:
              <LogsContainer></LogsContainer>
            </Space>
          </dd>
        </dl>
      </Card>
    </div>
  );
};

export default Home;
