import { Button, Empty } from 'antd';
import { Console, Hook, Unhook } from 'console-feed';
import { useEffect, useState } from 'react';

const LogsContainer = () => {
  const [logs, setLogs] = useState([]);

  // run once!
  useEffect(() => {
    Hook(
      window.console,
      (log) => {
        if (log.method === 'clear') {
          setLogs([]);
        } else {
          //@ts-ignore
          setLogs((currLogs) => [...currLogs, log]);
        }
        return;
      },
      false
    );
    return () => {
      //@ts-ignore
      Unhook(window.console);
    };
  }, []);

  return (
    <div className="border p-4 border-solid border-slate-400 flex flex-col">
      <div className="text-right">
        <Button
          className=""
          onClick={() => {
            console.clear();
          }}
        >
          Clear
        </Button>
      </div>
      <div className="overflow-auto mt-2" style={{ maxHeight: 500 }}>
        <Console
          logs={logs}
          variant="light"
          logFilter={(log: string) => {
            //ignore vite hot load messages
            if (log.includes('vite')) {
              return false;
            }
            return true;
          }}
        />
      </div>
      {logs.length === 0 && <Empty></Empty>}
    </div>
  );
};

export { LogsContainer };
